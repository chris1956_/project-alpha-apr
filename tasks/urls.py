from django.urls import include, path
from tasks.views import create_task, show_my_tasks

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("tasks/", include("tasks.urls")),
    path("mine/", show_my_tasks, name="show_my_tasks"),
]
